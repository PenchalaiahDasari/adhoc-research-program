package com.metagenics.seismic.reports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import com.metagenics.seismic.reports.config.SchedulerConfig;

 @Import({SchedulerConfig.class })
 @SpringBootApplication()
 @EntityScan("com.metagenics.seismic.reports*")
public class SeismicReportsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeismicReportsApplication.class, args);
	}
	
	 	@Bean
	    public RestTemplate restTemplate(RestTemplateBuilder builder) {
	        return builder.build();
	    }
}
