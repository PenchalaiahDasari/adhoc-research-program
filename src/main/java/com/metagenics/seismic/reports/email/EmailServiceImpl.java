/**
 * 
 */
package com.metagenics.seismic.reports.email;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.metagenics.seismic.reports.model.CommonResponseModel;

/**
 * @author PenchalaiahDasari
 *
 */
@Component
public class EmailServiceImpl{
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	public void sendReportsEmail(List<CommonResponseModel> responseList) throws MessagingException, IOException {
		logger.info("Entered in sendReportsEmail() of EmailServiceImpl..");
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(InternetAddress.parse("penchalaiahdasari@metagenics.com,ryanahn@metagenics.com"));
        helper.setSubject("Seismic Reports Status");
        String text ="<html><style>" + 
        		"table {" + 
        		"  font-family: arial, sans-serif;" + 
        		"  border-collapse: collapse;" + 
        		"  width: 100%;" + 
        		"}" + 
        		"" + 
        		"td, th {" + 
        		"  text-align: left;" + 
        		"  padding: 8px;" + 
        		"}" + 
        		"tr:nth-child(even) {" + 
        		"  background-color: #dddddd;" + 
        		"}" + 
        		"</style>" + 
        		"</head><body><h2>Seismic Reports Status</h2><table style=\"width:75%\">"
        		+ "<tr>" + 
        		"    <th width=25%>Report Name</th>" + 
        		"    <th width=25%>No. Of Records</th>" + 
        		"    <th width=25%>Status</th>" + 
        		"  </tr>\n";
        for(CommonResponseModel commonResponseModel:responseList) {
        	if(null!=commonResponseModel) {
	        	if((null!=commonResponseModel.getReportName()) && (null!=commonResponseModel.getStatus())) {
	        	 text=text+"<tr align='center'>"+"<td>" + commonResponseModel.getReportName().toUpperCase() + "</td>"
	                     + "<td>" + commonResponseModel.getCount() + "</td>"
	                     + "<td>" + commonResponseModel.getStatus().toUpperCase() + "</td>"
	        			 +"</tr>\n";
	        	}
        	}	
        }	
        text = text+"<tr></tr><tr></tr><tr>Regards\n</tr><tr>Metagenics - IT Team\n</tr><tr>email:it-support@metagenics.com</tr></table></body> </html>";
        helper.setText(text, true);
        javaMailSender.send(msg);
        logger.info("Exiting from  sendReportsEmail() of EmailServiceImpl..");
    }

}
