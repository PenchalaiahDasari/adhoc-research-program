package com.metagenics.seismic.reports.constants;

public class MetaConstants {
	
	 	public static final String TOKEN_URL = "https://auth.seismic.com/tenants/metagenics/connect/token#p";
	 	public static final String GRANT_TYPE="password";
	 	public static final String CLIENT_ID="6655b63f-529c-413b-bdef-b0b96b815d1f";
	 	public static final String CLIENT_SECRET="e9fbdb84-90a6-48f7-b8ca-95be90152fa2";
	 	public static final String USER_NAME = "patrickmoon@metagenics.com";
	 	public static final String PASSWORD="Patrick2!";
	 	public static final String SCOPE="reporting";
	 	public static final String TENANT ="metagenics";
	 	public static final String CONTENT_TYPE="application/x-www-form-urlencoded";
	 	public static final String REPORT_BASE_URL="https://api.seismic.com/reporting/v1/";
	 	//Reports
	 	public static final String USERS_REPORT ="users";
	 	public static final String LIBRARY_CONTENTS ="libraryContents";
	 	public static final String CONTENT_USAGE_HISTORY ="contentUsageHistory";
	 	public static final String CONTENT_VIEW_HISTORY ="contentViewHistory";
	 	public static final String SEARCH_HISTORY ="searchHistory";
	 	public static final String LIVE_SEND_LINKS ="livesendLinks";
	 	public static final String LIVE_SEND_LINK_CONTENTS ="livesendLinkContents";
	 	public static final String LIVE_SEND_LINK_MEMBERS ="livesendLinkMembers";
	 	public static final String LIVE_SEND_PAGE_VIEWS ="livesendPageViews";
	 	public static final String WORK_SPACE_CONTENTS ="workspaceContents";
}
