package com.metagenics.seismic.reports.service;

import java.sql.SQLException;

import org.springframework.http.ResponseEntity;

import com.metagenics.seismic.reports.model.AuthModel;

public interface SeismicReportsService {
	
	public ResponseEntity<AuthModel> getAccessToken();
	public String getReport(String token, String reportName) throws SQLException;
}
