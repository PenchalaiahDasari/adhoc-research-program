/**
 * 
 */
package com.metagenics.seismic.reports.service;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.metagenics.seismic.reports.constants.MetaConstants;
import com.metagenics.seismic.reports.dao.ISeismicReportsDAO;
import com.metagenics.seismic.reports.model.AuthModel;
import com.metagenics.seismic.reports.model.ContentUsageHistoryModel;
import com.metagenics.seismic.reports.model.ContentViewHistoryModel;
import com.metagenics.seismic.reports.model.LibraryContentsModel;
import com.metagenics.seismic.reports.model.LiveSendLinkContentsModel;
import com.metagenics.seismic.reports.model.LiveSendLinkMembersModel;
import com.metagenics.seismic.reports.model.LiveSendLinksModel;
import com.metagenics.seismic.reports.model.LiveSendPageViewsModel;
import com.metagenics.seismic.reports.model.SearchHistoryModel;
import com.metagenics.seismic.reports.model.UsersModel;
import com.metagenics.seismic.reports.model.WorkspaceContentsModel;

/**
 * @author PenchalaiahDasari
 *
 */
@Component
public class SeismicReportsServiceImpl implements SeismicReportsService {
	
		private final Logger logger = LoggerFactory.getLogger(getClass());
		@Autowired
	 	RestTemplate restTemplate;
		@Autowired
		ISeismicReportsDAO seismicReportsDAOImpl;
	 
	public ResponseEntity<AuthModel> getAccessToken() {
			logger.info("Entered in getAccessToken() of SeismicReportsServiceImpl..");
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", MetaConstants.CONTENT_TYPE);
			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
			map.add("grant_type", MetaConstants.GRANT_TYPE);
			map.add("client_id", MetaConstants.CLIENT_ID);
			map.add("client_secret", MetaConstants.CLIENT_SECRET);
			map.add("username", MetaConstants.USER_NAME);
			map.add("password", MetaConstants.PASSWORD);
			map.add("scope", MetaConstants.SCOPE);
			
	        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
	        System.out.println("Final Request String:"+request.toString());
	        ResponseEntity<AuthModel> response = restTemplate.exchange(MetaConstants.TOKEN_URL, HttpMethod.POST, request, AuthModel.class);
	        logger.info("Exiting from getAccessToken() of SeismicReportsServiceImpl..");
		return response;
	}
	
	 @Override
	 @Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public  String getReport(String token, String reportName) throws SQLException{
		logger.info("Entered in getReport() of SeismicReportsServiceImpl..");
		HttpHeaders headers = new HttpHeaders();
		String recCntAndstatus = null;
		headers.set("Content-Type", MetaConstants.CONTENT_TYPE);
		headers.set("Authorization", token);
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        
        System.out.println("Request to Seismic:"+request.toString());
		
        try {
			if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.USERS_REPORT)) {
				ResponseEntity<List<UsersModel>> usersList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<UsersModel>>() {});
				logger.info("usersList size::" +usersList.getBody().size());
				//Call to DB..
				recCntAndstatus = seismicReportsDAOImpl.saveUserReport(usersList);
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.LIBRARY_CONTENTS)) {
				ResponseEntity<List<LibraryContentsModel>> libraryContentsModelList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<LibraryContentsModel>>() {});
				logger.info("libraryContentsModelList size::" +libraryContentsModelList.getBody().size());
				//Call to DB..
				recCntAndstatus = seismicReportsDAOImpl.saveLibraryContentsReport(libraryContentsModelList);
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.CONTENT_USAGE_HISTORY)) {
				ResponseEntity<List<ContentUsageHistoryModel>> cntUsageHistoryList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<ContentUsageHistoryModel>>() {});
				logger.info("historyModelList size::" +cntUsageHistoryList.getBody().size());
				//Call to DB..
				 recCntAndstatus = seismicReportsDAOImpl.saveContentUsageHistoryReport(cntUsageHistoryList);
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.CONTENT_VIEW_HISTORY)) {
				ResponseEntity<List<ContentViewHistoryModel>> viewHistoryModelList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<ContentViewHistoryModel>>() {});
				//Call to DB..
				 recCntAndstatus = seismicReportsDAOImpl.saveContentViewHistoryReport(viewHistoryModelList);
				 logger.info("View historyModelList size::" +viewHistoryModelList.getBody().size());
				return recCntAndstatus;
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.SEARCH_HISTORY)) {
				ResponseEntity<List<SearchHistoryModel>> searchHistoryModelList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<SearchHistoryModel>>() {});
				//Call to DB..
				 recCntAndstatus = seismicReportsDAOImpl.saveSearchHistoryReport(searchHistoryModelList);
				 logger.info("searchHistoryModelList size::" +searchHistoryModelList.getBody().size());
				return recCntAndstatus;
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.LIVE_SEND_LINKS)) {
				ResponseEntity<List<LiveSendLinksModel>> liveSendLinksModelList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<LiveSendLinksModel>>() {});
				//Call to DB..
				 recCntAndstatus = seismicReportsDAOImpl.saveLiveSendLinksReport(liveSendLinksModelList);
				 logger.info("liveSendLinksModelList size::" +liveSendLinksModelList.getBody().size());
				return recCntAndstatus;
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.LIVE_SEND_LINK_CONTENTS)) {
				ResponseEntity<List<LiveSendLinkContentsModel>> liveSendLinkContentsModelList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<LiveSendLinkContentsModel>>() {});
				//Call to DB..
				 recCntAndstatus = seismicReportsDAOImpl.saveLiveSendLinkContentsReport(liveSendLinkContentsModelList);
				 logger.info("liveSendLinksModelList size::" +liveSendLinkContentsModelList.getBody().size());
				return recCntAndstatus;
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.LIVE_SEND_LINK_MEMBERS)) {
				ResponseEntity<List<LiveSendLinkMembersModel>> liveSendLinkMembersModelList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<LiveSendLinkMembersModel>>() {});
				//Call to DB..
				 recCntAndstatus = seismicReportsDAOImpl.saveLiveSendLinkMembersReport(liveSendLinkMembersModelList);
				 logger.info("liveSendLinkMembersModelList size::" +liveSendLinkMembersModelList.getBody().size());
				return recCntAndstatus;
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.LIVE_SEND_PAGE_VIEWS)) {
				ResponseEntity<List<LiveSendPageViewsModel>> liveSendPageViewsModelList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<LiveSendPageViewsModel>>() {});
				//Call to DB..
				 recCntAndstatus = seismicReportsDAOImpl.saveLiveSendPageViewsReport(liveSendPageViewsModelList);
				 logger.info("liveSendPageViewsModelList size::" +liveSendPageViewsModelList.getBody().size());
				return recCntAndstatus;
			}else if(null!=reportName && reportName.equalsIgnoreCase(MetaConstants.WORK_SPACE_CONTENTS)) {
				ResponseEntity<List<WorkspaceContentsModel>> workSpaceContentModelList = restTemplate.exchange(MetaConstants.REPORT_BASE_URL +reportName+"?", HttpMethod.GET, request, 
						new ParameterizedTypeReference<List<WorkspaceContentsModel>>() {});
				//Call to DB..
				 recCntAndstatus = seismicReportsDAOImpl.saveWorkSpaceContentReport(workSpaceContentModelList);
				 logger.info("workSpaceContentModelList size::" +workSpaceContentModelList.getBody().size());
				return recCntAndstatus;
			}
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        logger.info("Exiting from getReport() of SeismicReportsServiceImpl..");
        return recCntAndstatus;
	}
	
	

}
