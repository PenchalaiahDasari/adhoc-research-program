package com.metagenics.seismic.reports.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.metagenics.seismic.reports.model.ContentUsageHistoryModel;
import com.metagenics.seismic.reports.model.ContentViewHistoryModel;
import com.metagenics.seismic.reports.model.LibraryContentsModel;
import com.metagenics.seismic.reports.model.LiveSendLinkContentsModel;
import com.metagenics.seismic.reports.model.LiveSendLinkMembersModel;
import com.metagenics.seismic.reports.model.LiveSendLinksModel;
import com.metagenics.seismic.reports.model.LiveSendPageViewsModel;
import com.metagenics.seismic.reports.model.SearchHistoryModel;
import com.metagenics.seismic.reports.model.UsersModel;
import com.metagenics.seismic.reports.model.WorkspaceContentsModel;

public interface ISeismicReportsDAO {
	
	public String saveUserReport(ResponseEntity<List<UsersModel>> usersList) throws SQLException;
	public String saveContentUsageHistoryReport(ResponseEntity<List<ContentUsageHistoryModel>> cntUsageHistoryList)  throws SQLException;
	public String saveContentViewHistoryReport(ResponseEntity<List<ContentViewHistoryModel>> viewHistoryModelList);
	public String saveSearchHistoryReport(ResponseEntity<List<SearchHistoryModel>> searchHistoryModelList);
	public String saveLiveSendLinksReport(ResponseEntity<List<LiveSendLinksModel>> liveSendLinksModelList);
	public String saveLiveSendLinkContentsReport(ResponseEntity<List<LiveSendLinkContentsModel>> liveSendLinkContentsModelList);
	public String saveLiveSendLinkMembersReport(ResponseEntity<List<LiveSendLinkMembersModel>> liveSendLinkMembersModelList);
	public String saveLiveSendPageViewsReport(ResponseEntity<List<LiveSendPageViewsModel>> liveSendPageViewsModelList);
	public String saveWorkSpaceContentReport(ResponseEntity<List<WorkspaceContentsModel>> workSpaceContentModelList);
	public String saveLibraryContentsReport(ResponseEntity<List<LibraryContentsModel>> libraryContentsModelList);

}
