package com.metagenics.seismic.reports.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.metagenics.seismic.reports.model.ContentUsageHistoryModel;
import com.metagenics.seismic.reports.model.ContentViewHistoryModel;
import com.metagenics.seismic.reports.model.LibraryContentsModel;
import com.metagenics.seismic.reports.model.LiveSendLinkContentsModel;
import com.metagenics.seismic.reports.model.LiveSendLinkMembersModel;
import com.metagenics.seismic.reports.model.LiveSendLinksModel;
import com.metagenics.seismic.reports.model.LiveSendPageViewsModel;
import com.metagenics.seismic.reports.model.SearchHistoryModel;
import com.metagenics.seismic.reports.model.UsersModel;
import com.metagenics.seismic.reports.model.WorkspaceContentsModel;

@Component
public class SeismicReportsDAOImpl implements ISeismicReportsDAO{
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private DataSource dataSource;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSource);
	}
	
	/**************USER_REPORT *************/
	public String saveUserReport(ResponseEntity<List<UsersModel>> usersList) {
		logger.info("Entered in saveUserReport() of SeismicReportsDAOImpl..");
		 long start = System.currentTimeMillis();
		String recCntAndstatus = null;
		try {
			
			/******************* Truncate the Existing records   *************/
			
			String deleteSQL = " delete from ods.sm.users";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveUserReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to USERS table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.users values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							UsersModel user = (usersList.getBody().get(i));
							 pstmt.setString(1, user.getId());
				                pstmt.setString(2, user.getUsername());
				                pstmt.setString(3, user.getEmail());
				                pstmt.setString(4, user.getEmailDomain());
				                pstmt.setString(5, user.getFullName());
				                pstmt.setString(6, user.getFirstName());
				                pstmt.setString(7, user.getLastName());
				                pstmt.setString(8, user.getCreatedAt());
				                pstmt.setString(9, user.getModifiedAt());
				                pstmt.setString(10, user.getOrganization());
				                pstmt.setString(11, user.getTitle());
				                pstmt.setString(12,user.getIsDeleted());
				                pstmt.setString(13, user.getLicenseType());
				                pstmt.setString(14, user.getIsSeismicEmployee());
				                pstmt.setString(15,user.getIsSystemAdmin());
				                pstmt.setString(16, "");//defaultContententProfiled - Not getting values from Seismic
				                pstmt.setString(20, user.getDeletedAt());
				                //TODO: For Groups - mapping to be done
				                pstmt.setString(17, "");
				                pstmt.setString(18, "");
				                pstmt.setString(19, "");
						}
 
						public int getBatchSize() {
							return usersList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("1....Finished. Time taken : " + (end - start) + " milli seconds.");
		logger.info("Exiting from saveUserReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	
	
	/*
	 * public String saveUserReport(ResponseEntity<List<UsersModel>> usersList)
	 * throws SQLException {
	 * logger.info("Entered in saveUserReport() of SeismicReportsDAOImpl.."); int[]
	 * countArray=null; Connection con = null; String recCntAndstatus = null; try {
	 * con = getSeismicConnection(); Statement stmt = con.createStatement();
	 * PreparedStatement pstmt = con.
	 * prepareStatement("insert into ods.sm.users values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
	 * ); // start transaction block con.setAutoCommit(false); // default true
	 * 
	 * String deleteRecords = "delete from ods.sm.users";
	 * stmt.execute(deleteRecords);
	 * 
	 * System.out.println("Starting batch operation using Bulk Copy API."); long
	 * start = System.currentTimeMillis(); for (UsersModel user:
	 * usersList.getBody()) { pstmt.setString(1, user.getId()); pstmt.setString(2,
	 * user.getUsername()); pstmt.setString(3, user.getEmail()); pstmt.setString(4,
	 * user.getEmailDomain()); pstmt.setString(5, user.getFullName());
	 * pstmt.setString(6, user.getFirstName()); pstmt.setString(7,
	 * user.getLastName()); pstmt.setString(8, user.getCreatedAt());
	 * pstmt.setString(9, user.getModifiedAt()); pstmt.setString(10,
	 * user.getOrganization()); pstmt.setString(11, user.getTitle());
	 * pstmt.setString(12,user.getIsDeleted()); pstmt.setString(13,
	 * user.getLicenseType()); pstmt.setString(14, user.getIsSeismicEmployee());
	 * pstmt.setString(15,user.getIsSystemAdmin()); pstmt.setString(16,
	 * "");//defaultContententProfiled - Not getting values from Seismic
	 * pstmt.setString(20, user.getDeletedAt()); //TODO: For Groups - mapping to be
	 * done pstmt.setString(17, ""); pstmt.setString(18, ""); pstmt.setString(19,
	 * ""); pstmt.addBatch(); } countArray = pstmt.executeBatch(); // End
	 * transaction block, commit changes con.commit(); con.setAutoCommit(true);//set
	 * back to true, good practice
	 * 
	 * recCntAndstatus = countArray.length+","+"Success"; long end =
	 * System.currentTimeMillis(); System.out.println("Finished. Time taken : " +
	 * (end - start) + " milliseconds."); }catch(SQLException ex) { con.rollback();
	 * recCntAndstatus = "0,"+"FAILURE"; ex.printStackTrace(); }
	 * logger.info("Exiting from saveUserReport() of SeismicReportsDAOImpl..");
	 * return recCntAndstatus; }
	 */
	
	/**************CONTENT_USAGE_HISTORY_REPORT *************/
	public String saveContentUsageHistoryReport(ResponseEntity<List<ContentUsageHistoryModel>> cntUsageHistoryList) {
		logger.info("Entered in saveContentUsageHistoryReport() of SeismicReportsDAOImpl..");
		String recCntAndstatus = null;
		  long start = System.currentTimeMillis();
		try {
			/******************* Truncate the Existing records   *************/
			String deleteSQL = " delete from ods.sm.content_usage_history";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveContentUsageHistoryReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to CONTENT_USAGE_HISTORY table ***************/
				int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.content_usage_history values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							ContentUsageHistoryModel cntUsageHistory = (cntUsageHistoryList.getBody().get(i));
							  pstmt.setString(1, cntUsageHistory.getId());
				                pstmt.setString(2, cntUsageHistory.getOccurredAt());
				                pstmt.setString(3, cntUsageHistory.getAction());
				                pstmt.setString(4, cntUsageHistory.getActionType());
				                pstmt.setString(5, cntUsageHistory.getIsBoundDelivery());
				                pstmt.setString(6, cntUsageHistory.getUserId());
				                pstmt.setString(7, cntUsageHistory.getInstanceName());
				                pstmt.setString(8, "NA");
				                pstmt.setString(9, cntUsageHistory.getLibraryContentId());
				                pstmt.setString(10, cntUsageHistory.getLibraryContentName());
				                pstmt.setString(11, cntUsageHistory.getLibraryContentVersionId());
				                pstmt.setString(12,cntUsageHistory.getContentProfileId());
				                pstmt.setString(13, "");//liveSendLinkContentId - not getting from seismic
				                pstmt.setString(14, "");//WorkspaceId
				                pstmt.setString(15,"");//Workspace Content Version Id
						}
 
						public int getBatchSize() {
							return cntUsageHistoryList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE";
			e.printStackTrace();
		}
		
		 long end = System.currentTimeMillis();
		 logger.info("2....Finished. Time taken : " + (end - start)+ " milli seconds.");
 		logger.info("Exiting from saveContentUsageHistoryReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	/**************CONTENT_VIEW_HISTORY_REPORT *************/
	public String saveContentViewHistoryReport(ResponseEntity<List<ContentViewHistoryModel>> viewHistoryModelList) {
		logger.info("Entered in saveContentViewHistoryReport() of SeismicReportsDAOImpl..");
		String recCntAndstatus = null;
		  long start = System.currentTimeMillis();
		try {
			
			/******************* Truncate the Existing records   *************/
			String deleteSQL = " delete from ods.sm.content_view_history";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveContentViewHistoryReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to CONTENT_VIEW_HISTORY table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.content_view_history values (?,?,?,?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							ContentViewHistoryModel cntViewHistory = (viewHistoryModelList.getBody().get(i));
							  pstmt.setString(1, cntViewHistory.getId());
				                pstmt.setString(2, cntViewHistory.getOccurredAt());
				                pstmt.setString(3, cntViewHistory.getAction());
				                pstmt.setString(4, cntViewHistory.getUserId());
				                pstmt.setString(5, cntViewHistory.getInstanceName());
				                pstmt.setString(6, "");//libraryContentId
				                pstmt.setString(7, "");
				                pstmt.setString(8, "");
				                pstmt.setString(9, cntViewHistory.getWorkspaceCntntId());
				                pstmt.setString(10, cntViewHistory.getWorkspaceCntntVersionId());
						}
						public int getBatchSize() {
							return viewHistoryModelList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE"+"("+e.getRootCause()+")";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("3....Finished. Time taken : " + (end - start) + " milli seconds.");
 		logger.info("Exiting from saveContentViewHistoryReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	/**************SEARCH_HISTORY_REPORT *************/
	public String saveSearchHistoryReport(ResponseEntity<List<SearchHistoryModel>> searchHistoryModelList) {
		logger.info("Entered in saveSearchHistoryReport() of SeismicReportsDAOImpl..");
		String recCntAndstatus = null;
		  long start = System.currentTimeMillis();
		try {
			
			/******************* Truncate the Existing records   *************/
			String deleteSQL = " delete from ods.sm.search_history";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveSearchHistoryReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to search_history table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.search_history values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							SearchHistoryModel searchHistoryModel = (searchHistoryModelList.getBody().get(i));
							  pstmt.setString(1, searchHistoryModel.getId());
				                pstmt.setString(2, searchHistoryModel.getSearchCycleId());
				                pstmt.setString(3, searchHistoryModel.getSearchTermRaw());
				                pstmt.setString(4, searchHistoryModel.getSearchTermNormalized());
				                pstmt.setString(5, searchHistoryModel.getSearchType());
				                pstmt.setString(6, searchHistoryModel.getSortBy());
				                pstmt.setString(7, searchHistoryModel.getOccurredAt());
				                pstmt.setString(8, searchHistoryModel.getUserId());
				                pstmt.setString(9, searchHistoryModel.getResultCount());
				                pstmt.setString(10, searchHistoryModel.getResultCountDocCenter());
				                pstmt.setString(11, searchHistoryModel.getResultCountWorkspace());
				                pstmt.setString(12, searchHistoryModel.getResultCountContentManager());
				                pstmt.setString(13, searchHistoryModel.getSelectedFacetsInternal20Only());
				                pstmt.setString(14, searchHistoryModel.getSelectedFacetsTeam());
				                pstmt.setString(15, searchHistoryModel.getSelectedFacetsTopic());
						}
						public int getBatchSize() {
							return searchHistoryModelList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE"+"("+e.getRootCause()+")";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("4....Finished. Time taken : " + (end - start) + " milli seconds.");
 		logger.info("Exiting from saveSearchHistoryReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	/**************LIVE_SEND_LINKS_REPORT *************/
	public String saveLiveSendLinksReport(ResponseEntity<List<LiveSendLinksModel>> liveSendLinksModelList) {
		logger.info("Entered in saveLiveSendLinksReport() of SeismicReportsDAOImpl..");
		String recCntAndstatus = null;
		  long start = System.currentTimeMillis();
		try {
			/******************* Truncate the Existing records   *************/
			String deleteSQL = " delete from ods.sm.livesend_links";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveLiveSendLinksReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to livesend_links table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.livesend_links values (?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							LiveSendLinksModel liveSendLinksModel = (liveSendLinksModelList.getBody().get(i));
							  pstmt.setString(1, liveSendLinksModel.getId());
				                pstmt.setString(2, liveSendLinksModel.getIsSeparateSend());
				                pstmt.setString(3, liveSendLinksModel.getNotificationMode());
				                pstmt.setString(4, liveSendLinksModel.getCreatedAt());
				                pstmt.setString(5, liveSendLinksModel.getModifiedAt());
				                pstmt.setString(6, liveSendLinksModel.getCreatedBy());
				                pstmt.setString(7, liveSendLinksModel.getCreatedByUsername());
						}
						public int getBatchSize() {
							return liveSendLinksModelList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE"+"("+e.getRootCause()+")";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("5....Finished. Time taken : " + (end - start) + " milli seconds.");
 		logger.info("Exiting from saveLiveSendLinksReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	/**************LIVE_SEND_LINK_CONTENTS_REPORT *************/
	public String saveLiveSendLinkContentsReport(ResponseEntity<List<LiveSendLinkContentsModel>> liveSendLinkContentsModelList) {
		logger.info("Entered in saveLiveSendLinkContentsReport() of SeismicReportsDAOImpl..");
		String recCntAndstatus = null;
		  long start = System.currentTimeMillis();
		try {
			/******************* Truncate the Existing records   *************/
			String deleteSQL = " delete from ods.sm.livesend_link_contents";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveLiveSendLinkContentsReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to livesend_link_contents table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.livesend_link_contents values (?,?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							LiveSendLinkContentsModel liveSendLinkContentsModel = (liveSendLinkContentsModelList.getBody().get(i));
							  pstmt.setString(1, liveSendLinkContentsModel.getId());
				                pstmt.setString(2, liveSendLinkContentsModel.getLivesendLinkId());
				                pstmt.setString(3, liveSendLinkContentsModel.getName());
				                pstmt.setString(4, liveSendLinkContentsModel.getCreatedAt());
				                pstmt.setString(5, liveSendLinkContentsModel.getModifiedAt());
				                pstmt.setString(6, liveSendLinkContentsModel.getCreatedBy());
				                pstmt.setString(7, liveSendLinkContentsModel.getCreatedByUsername());
				                pstmt.setInt(8, liveSendLinkContentsModel.getTotalPages());
						}
						public int getBatchSize() {
							return liveSendLinkContentsModelList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE"+"("+e.getRootCause()+")";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("6....Finished. Time taken : " + (end - start) + " milli seconds.");
 		logger.info("Exiting from saveLiveSendLinkContentsReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	/**************LIVE_SEND_LINK_MEMBERS_REPORT *************/
	public String saveLiveSendLinkMembersReport(ResponseEntity<List<LiveSendLinkMembersModel>> liveSendLinkMembersModelList) {
		logger.info("Entered in saveLiveSendLinkMembersReport() of SeismicReportsDAOImpl..");
		String recCntAndstatus = null;
		  long start = System.currentTimeMillis();
		try {
			/******************* Truncate the Existing records   *************/
			String deleteSQL = " delete from ods.sm.livesend_link_members";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveLiveSendLinkMembersReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to livesend_link_members table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.livesend_link_members values (?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							LiveSendLinkMembersModel LiveSendLinkMembersModel = (liveSendLinkMembersModelList.getBody().get(i));
							  pstmt.setString(1, LiveSendLinkMembersModel.getLivesendLinkId());
				                pstmt.setString(2, LiveSendLinkMembersModel.getEmail());
				                pstmt.setString(3, LiveSendLinkMembersModel.getModifiedAt());
				                
						}
						public int getBatchSize() {
							return liveSendLinkMembersModelList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE"+"("+e.getRootCause()+")";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("7....Finished. Time taken : " + (end - start) + " milli seconds.");
 		logger.info("Exiting from saveLiveSendLinkMembersReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	/**************LIVE_SEND_PAGE_VIEWS_REPORT *************/
	public String saveLiveSendPageViewsReport(ResponseEntity<List<LiveSendPageViewsModel>> liveSendPageViewsModelList) {
		logger.info("Entered in saveLiveSendPageViewsReport() of SeismicReportsDAOImpl..");
		String recCntAndstatus = null;
		  long start = System.currentTimeMillis();
		try {
			/******************* Truncate the Existing records   *************/
			String deleteSQL = "delete from ods.sm.livesend_page_views";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveLiveSendPageViewsReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to livesend_page_views table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.livesend_page_views values (?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							LiveSendPageViewsModel liveSendPageViewsModel = (liveSendPageViewsModelList.getBody().get(i));
							  pstmt.setString(1, liveSendPageViewsModel.getOccurredAt());
				                pstmt.setInt(2, liveSendPageViewsModel.getPageIndex());
				                pstmt.setString(3, liveSendPageViewsModel.getLivesendLinkContentId());
				                pstmt.setInt(4, liveSendPageViewsModel.getDuration());
				                pstmt.setString(5, liveSendPageViewsModel.getLivesendViewingSessionId());
				                pstmt.setString(6, liveSendPageViewsModel.getSessionStartedAt());
				                pstmt.setString(7, liveSendPageViewsModel.getSessionEndedAt());
				                
						}
						public int getBatchSize() {
							return liveSendPageViewsModelList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE"+"("+e.getRootCause()+")";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("8....Finished. Time taken : " + (end - start) + " milli seconds.");
 		logger.info("Exiting from saveLiveSendPageViewsReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	/************** WORK_SPACE_CONTENT_REPORT *************/
	public String saveWorkSpaceContentReport(ResponseEntity<List<WorkspaceContentsModel>> workSpaceContentModelList) {
		logger.info("Entered in saveWorkSpaceContentReport() of SeismicReportsDAOImpl..");
		String recCntAndstatus = null;
		  long start = System.currentTimeMillis();
		try {
			
			/******************* Truncate the Existing records   *************/
			String deleteSQL = " delete from ods.sm.workspace_contents";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveWorkSpaceContentReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to workspace_contents table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.workspace_contents values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							WorkspaceContentsModel workspaceContentsModel = (workSpaceContentModelList.getBody().get(i));
							  	pstmt.setString(1, workspaceContentsModel.getId());
				                pstmt.setString(2, workspaceContentsModel.getName());
				                pstmt.setString(3, workspaceContentsModel.getCreatedBy());
				                pstmt.setString(4, workspaceContentsModel.getCreatedAt());
				                pstmt.setString(5, workspaceContentsModel.getModifiedAt());
				                pstmt.setString(6, workspaceContentsModel.getIsDeleted());
				                pstmt.setString(7, workspaceContentsModel.getLatestWrkspaceCntVersionId());
				                pstmt.setString(8, workspaceContentsModel.getLatestWrkspaceCntVersionCreatedAt());
				                pstmt.setString(9, workspaceContentsModel.getIsContextualFolderContent());
				                pstmt.setString(10, workspaceContentsModel.getIsFolder());
				                pstmt.setString(11, workspaceContentsModel.getIsCartContent());
				                pstmt.setString(12, workspaceContentsModel.getLibraryContentId());
				                pstmt.setString(13, workspaceContentsModel.getOriginContentProfileId());
				                pstmt.setInt(14, workspaceContentsModel.getVersion());
				                pstmt.setString(15, workspaceContentsModel.getMaterializedPath());
						}
						public int getBatchSize() {
							return workSpaceContentModelList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE"+"("+e.getRootCause()+")";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("9....Finished. Time taken : " + (end - start) + " milli seconds.");
 		logger.info("Exiting from saveWorkSpaceContentReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	/**************LIBRARY_CONTENTS_REPORT *************/
	public String saveLibraryContentsReport(ResponseEntity<List<LibraryContentsModel>> libraryContentsModelList) {
		logger.info("Entered in saveLibraryContentsReport() of SeismicReportsDAOImpl..");
		 long start = System.currentTimeMillis();
		String recCntAndstatus = null;
		try {
			
			/******************* Truncate the Existing records   *************/
			
			String deleteSQL = " delete from ods.sm.library_contents";
			int rowsEffected = jdbcTemplate.update(deleteSQL);
			logger.info("No.of rows effected in saveUserReport() of SeismicReportsDAOImpl.." +rowsEffected);
			
			/******* Insert the incoming API records in to USERS table ***************/
			int[] count=jdbcTemplate.batchUpdate(
					"insert into ods.sm.library_contents values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
					+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
					new BatchPreparedStatementSetter() {
						public void setValues(PreparedStatement pstmt, int i) throws SQLException {
							LibraryContentsModel libraryContentsModel = (libraryContentsModelList.getBody().get(i));
							 	pstmt.setString(1, libraryContentsModel.getId());
				                pstmt.setString(2, libraryContentsModel.getName());
				                pstmt.setString(3, libraryContentsModel.getVersion());
				                pstmt.setString(4, libraryContentsModel.getCreatedAt());
				                pstmt.setString(5, libraryContentsModel.getModifiedAt());
				                pstmt.setString(6, libraryContentsModel.getLatestLibraryCntntVersionId());
				                pstmt.setInt(7, libraryContentsModel.getLatestLibraryCntntVersionSize());
				                pstmt.setString(8, libraryContentsModel.getLatestLibraryCntntVersionCreatedAt());
				                pstmt.setString(9, libraryContentsModel.getLibraryUrl());
				                pstmt.setString(10, libraryContentsModel.getDocCenterUrl());
				                pstmt.setString(11, libraryContentsModel.getFormat());
				                pstmt.setString(12,libraryContentsModel.getTeamsiteId());
				                pstmt.setString(13, libraryContentsModel.getIsDeleted());
				                pstmt.setString(14, libraryContentsModel.getOwnerId());
				                pstmt.setString(15,libraryContentsModel.getIsPublished());
				                pstmt.setString(16, "");
				                pstmt.setString(17, "");
				                pstmt.setString(18, "");
				                pstmt.setString(19, "");
				                pstmt.setString(20, "");
				                pstmt.setString(21, "");
				                pstmt.setString(22, "");
				                pstmt.setString(23, "");
				                pstmt.setString(24, "");
				                pstmt.setString(25, "");
				                pstmt.setString(26, "");
				                pstmt.setString(27, "");
				                pstmt.setString(28, "");
				                pstmt.setString(29, "");
				                pstmt.setString(30, "");
				                pstmt.setString(31, "");
				                pstmt.setString(32,"");
				                pstmt.setString(33, "");
				                pstmt.setString(34, "");
				                pstmt.setString(35,"");
						}
 
						public int getBatchSize() {
							return libraryContentsModelList.getBody().size();
						}
					});
			recCntAndstatus=count.length+","+"Success";
			
		} catch (DataAccessException e) {
			 recCntAndstatus = "0,"+"FAILURE";
			e.printStackTrace();
		}
		 long end = System.currentTimeMillis();
		 logger.info("10....Finished. Time taken : " + (end - start) + " milli seconds.");
		logger.info("Exiting from saveLibraryContentsReport() of SeismicReportsDAOImpl..");
		return recCntAndstatus;
	}
	
	public Connection getSeismicConnection() {
		Connection connection = null;
		try {
			String url = "jdbc:sqlserver://sf.metagenics.com;databaseName=ODS;user=seismic;password=seismic";
			connection = DriverManager.getConnection(url + ";useBulkCopyForBatchInsert=true"); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
		
}


