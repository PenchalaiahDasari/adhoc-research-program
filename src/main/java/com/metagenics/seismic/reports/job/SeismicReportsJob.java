/**
 * 
 */
package com.metagenics.seismic.reports.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.metagenics.seismic.reports.controller.SeismicReportsController;

/**
 * @author PenchalaiahDasari
 *
 */
public class SeismicReportsJob implements Job{
	
	@Autowired
    private SeismicReportsController seismicReportsController;
 
    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
    	seismicReportsController.getReports("");
    }

}
