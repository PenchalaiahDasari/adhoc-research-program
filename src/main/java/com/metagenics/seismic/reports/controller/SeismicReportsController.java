/**
 * 
 */
package com.metagenics.seismic.reports.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;
import javax.ws.rs.QueryParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metagenics.seismic.reports.email.EmailServiceImpl;
import com.metagenics.seismic.reports.model.AuthModel;
import com.metagenics.seismic.reports.model.CommonResponseModel;
import com.metagenics.seismic.reports.service.SeismicReportsService;

/**
 * @author PenchalaiahDasari
 *
 */
@RestController
@RequestMapping(value = "/v1")
public class SeismicReportsController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	SeismicReportsService seismicReportsServiceImpl;
	@Autowired
	EmailServiceImpl emailService;
	 
	@RequestMapping(value = "/accessToken", method = RequestMethod.GET, produces = "application/json")
	public AuthModel getAccessToken() {
		logger.info("Entered in SeismicReportsController..");
		AuthModel authModel = (AuthModel)seismicReportsServiceImpl.getAccessToken().getBody();
		logger.info("Exiting from SeismicReportsController..");
		return authModel;
	}
	
	@RequestMapping(value = "/reports", method = RequestMethod.GET, produces = "application/json")
	public List<CommonResponseModel> getReports(@QueryParam("reportNames") String reportNames) {
		logger.info("Entered in getReports() of SeismicReportsController..reportName:" +reportNames);
		reportNames= "users,contentUsageHistory,contentViewHistory,searchHistory,livesendLinks,livesendLinkContents,livesendLinkMembers,livesendPageViews,workspaceContents,libraryContents";
		CommonResponseModel commonResponseModel = null;
		List<CommonResponseModel> responseList= new ArrayList<CommonResponseModel>();
		String recCntAndStatus =null;
		String[] reports = reportNames.split(",");
	    List<String> reportsList = Arrays.asList(reports);
	    
	    /****** Get the Auth Token ***********/
		AuthModel authModel = (AuthModel)seismicReportsServiceImpl.getAccessToken().getBody();
		
		/****** Got The Token, Calling Reports ***********/
		for(String reportName:reportsList) {
			try {
				recCntAndStatus = seismicReportsServiceImpl.getReport("Bearer "+authModel.getAccess_token(), reportName);
				commonResponseModel = prepareFinalResponseList(recCntAndStatus, reportName);
				responseList.add(commonResponseModel);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		/********* Sending Email Report ***********************/
		  try { 
				  emailService.sendReportsEmail(responseList); 
		  	} catch (MessagingException | IOException e){ 
			 e.printStackTrace(); 
			 }
		logger.info("Exiting from getReports() of SeismicReportsController..");
		return responseList;
	}
	
	private  CommonResponseModel prepareFinalResponseList(String recCntAndstatus, String reportName){
		 CommonResponseModel responseModel = null;
		if(null!=recCntAndstatus) {
		responseModel = new CommonResponseModel();
   		String[] split = recCntAndstatus.split(",");
       	responseModel.setReportName(reportName);
       	responseModel.setCount(split[0]);
       	responseModel.setStatus(split[1]);
   	}
		return responseModel;
	}

}
