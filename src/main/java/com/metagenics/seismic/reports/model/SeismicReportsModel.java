/**
 * 
 */
package com.metagenics.seismic.reports.model;

/**
 * @author PenchalaiahDasari
 *
 */
public class SeismicReportsModel {
	
	private String grantType;
	private String clientId;
	private String clientSecret;
	private String userName;
	private String password;
	private String scope;
	
	public String getGrantType() {
		return grantType;
	}
	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientSecret() {
		return clientSecret;
	}
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	
	@Override
	public String toString() {
		return "SeismicReportsModel [grantType=" + grantType + ", clientId=" + clientId + ", clientSecret="
				+ clientSecret + ", userName=" + userName + ", password=" + password + ", scope=" + scope + "]";
	}
	
	
	

}
