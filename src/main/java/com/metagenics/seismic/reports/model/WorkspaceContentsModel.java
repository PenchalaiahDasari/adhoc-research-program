/**
 * 
 */
package com.metagenics.seismic.reports.model;

/**
 * @author PenchalaiahDasari
 *
 */
public class WorkspaceContentsModel {
	
	private String id;
	private String name;
	private String createdBy;
	private String createdAt;
	private String modifiedAt;
	private String isDeleted;
	private String latestWrkspaceCntVersionId;
	private String latestWrkspaceCntVersionCreatedAt; 
	private String isContextualFolderContent;
	private String isFolder;
	private String isCartContent;
	private String libraryContentId;
	private String originContentProfileId;
	private int version;
	private String materializedPath;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(String modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getLatestWrkspaceCntVersionId() {
		return latestWrkspaceCntVersionId;
	}
	public void setLatestWrkspaceCntVersionId(String latestWrkspaceCntVersionId) {
		this.latestWrkspaceCntVersionId = latestWrkspaceCntVersionId;
	}
	public String getLatestWrkspaceCntVersionCreatedAt() {
		return latestWrkspaceCntVersionCreatedAt;
	}
	public void setLatestWrkspaceCntVersionCreatedAt(String latestWrkspaceCntVersionCreatedAt) {
		this.latestWrkspaceCntVersionCreatedAt = latestWrkspaceCntVersionCreatedAt;
	}
	public String getIsContextualFolderContent() {
		return isContextualFolderContent;
	}
	public void setIsContextualFolderContent(String isContextualFolderContent) {
		this.isContextualFolderContent = isContextualFolderContent;
	}
	public String getIsFolder() {
		return isFolder;
	}
	public void setIsFolder(String isFolder) {
		this.isFolder = isFolder;
	}
	public String getIsCartContent() {
		return isCartContent;
	}
	public void setIsCartContent(String isCartContent) {
		this.isCartContent = isCartContent;
	}
	public String getLibraryContentId() {
		return libraryContentId;
	}
	public void setLibraryContentId(String libraryContentId) {
		this.libraryContentId = libraryContentId;
	}
	public String getOriginContentProfileId() {
		return originContentProfileId;
	}
	public void setOriginContentProfileId(String originContentProfileId) {
		this.originContentProfileId = originContentProfileId;
	}
	public String getMaterializedPath() {
		return materializedPath;
	}
	public void setMaterializedPath(String materializedPath) {
		this.materializedPath = materializedPath;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
}
