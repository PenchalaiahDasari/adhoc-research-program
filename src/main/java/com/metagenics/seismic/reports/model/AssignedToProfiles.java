/**
 * 
 */
package com.metagenics.seismic.reports.model;

/**
 * @author PenchalaiahDasari
 *
 */
public class AssignedToProfiles {
	
	private String id;
	private String name;
	private String profileType;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProfileType() {
		return profileType;
	}
	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}
	
	

}
