package com.metagenics.seismic.reports.model;

public class PropertiesModel {

	private String team;
	private String internalOnly;
	private String topic;
	
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public String getInternalOnly() {
		return internalOnly;
	}
	public void setInternalOnly(String internalOnly) {
		this.internalOnly = internalOnly;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	
}
