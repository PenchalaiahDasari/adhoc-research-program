/**
 * 
 */
package com.metagenics.seismic.reports.model;

/**
 * @author PenchalaiahDasari
 *
 */
public class ContentUsageHistoryModel {
	private String id;
	private String occurredAt;
	private String action;
	private String actionType;
	private String isBoundDelivery;
	private String userId;
	private String instanceName;
	private String instanceFormat;
	private String libraryContentId;
	private String libraryContentName;
	private String libraryContentVersionId;
	private String contentProfileId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOccurredAt() {
		return occurredAt;
	}
	public void setOccurredAt(String occurredAt) {
		this.occurredAt = occurredAt;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getInstanceFormat() {
		return instanceFormat;
	}
	public void setInstanceFormat(String instanceFormat) {
		this.instanceFormat = instanceFormat;
	}
	public String getLibraryContentId() {
		return libraryContentId;
	}
	public void setLibraryContentId(String libraryContentId) {
		this.libraryContentId = libraryContentId;
	}
	public String getLibraryContentName() {
		return libraryContentName;
	}
	public void setLibraryContentName(String libraryContentName) {
		this.libraryContentName = libraryContentName;
	}
	public String getLibraryContentVersionId() {
		return libraryContentVersionId;
	}
	public void setLibraryContentVersionId(String libraryContentVersionId) {
		this.libraryContentVersionId = libraryContentVersionId;
	}
	public String getContentProfileId() {
		return contentProfileId;
	}
	public void setContentProfileId(String contentProfileId) {
		this.contentProfileId = contentProfileId;
	}
	public String getIsBoundDelivery() {
		return isBoundDelivery;
	}
	public void setIsBoundDelivery(String isBoundDelivery) {
		this.isBoundDelivery = isBoundDelivery;
	}
	
	

}
