package com.metagenics.seismic.reports.model;

public class LiveSendPageViewsModel {
	
	private String occurredAt;
	private int pageIndex;
	private String livesendLinkContentId;
	private int duration;
	private String livesendViewingSessionId;
	private String sessionStartedAt;
	private String sessionEndedAt;
	
	public String getOccurredAt() {
		return occurredAt;
	}
	public void setOccurredAt(String occurredAt) {
		this.occurredAt = occurredAt;
	}
	public int getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	public String getLivesendLinkContentId() {
		return livesendLinkContentId;
	}
	public void setLivesendLinkContentId(String livesendLinkContentId) {
		this.livesendLinkContentId = livesendLinkContentId;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public String getSessionStartedAt() {
		return sessionStartedAt;
	}
	public void setSessionStartedAt(String sessionStartedAt) {
		this.sessionStartedAt = sessionStartedAt;
	}
	public String getSessionEndedAt() {
		return sessionEndedAt;
	}
	public void setSessionEndedAt(String sessionEndedAt) {
		this.sessionEndedAt = sessionEndedAt;
	}
	public String getLivesendViewingSessionId() {
		return livesendViewingSessionId;
	}
	public void setLivesendViewingSessionId(String livesendViewingSessionId) {
		this.livesendViewingSessionId = livesendViewingSessionId;
	}
	
	
		
}
