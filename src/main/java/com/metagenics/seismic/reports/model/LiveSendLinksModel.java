package com.metagenics.seismic.reports.model;

public class LiveSendLinksModel {
	
	private String id;
	private String isSeparateSend;
	private String notificationMode;
	private String createdAt;
	private String modifiedAt;
	private String createdBy;
	private String createdByUsername;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIsSeparateSend() {
		return isSeparateSend;
	}
	public void setIsSeparateSend(String isSeparateSend) {
		this.isSeparateSend = isSeparateSend;
	}
	public String getNotificationMode() {
		return notificationMode;
	}
	public void setNotificationMode(String notificationMode) {
		this.notificationMode = notificationMode;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(String modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedByUsername() {
		return createdByUsername;
	}
	public void setCreatedByUsername(String createdByUsername) {
		this.createdByUsername = createdByUsername;
	}
}
