/**
 * 
 */
package com.metagenics.seismic.reports.model;

import java.util.List;

/**
 * @author PenchalaiahDasari
 *
 */
public class UsersModel {

	private String id;
	private String username;
	private String email;
	private String emailDomain;
	private String fullName;
	private String firstName;
	private String lastName;
	private String createdAt;
	private String modifiedAt;
	private String deletedAt;
	private String organization;
	private String title;
	private String isDeleted;
	private String licenseType;
	private String isSeismicEmployee;
	private String isSystemAdmin;
	private List<String> groups;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailDomain() {
		return emailDomain;
	}
	public void setEmailDomain(String emailDomain) {
		this.emailDomain = emailDomain;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(String modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public String getDeletedAt() {
		return deletedAt;
	}
	public void setDeletedAt(String deletedAt) {
		this.deletedAt = deletedAt;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	public String getIsSeismicEmployee() {
		return isSeismicEmployee;
	}
	public void setIsSeismicEmployee(String isSeismicEmployee) {
		this.isSeismicEmployee = isSeismicEmployee;
	}
	public String getIsSystemAdmin() {
		return isSystemAdmin;
	}
	public void setIsSystemAdmin(String isSystemAdmin) {
		this.isSystemAdmin = isSystemAdmin;
	}
	public List<String> getGroups() {
		return groups;
	}
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}
	
	
	
}
