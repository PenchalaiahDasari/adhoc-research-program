/**
 * 
 */
package com.metagenics.seismic.reports.model;

import java.util.List;

/**
 * @author PenchalaiahDasari
 *
 */
public class LibraryContentsModel {
	private String id;
	private String name;
	private String version;
	private String createdAt;
	private String modifiedAt;
	private String latestLibraryCntntVersionId;
	private int latestLibraryCntntVersionSize;
	private String latestLibraryCntntVersionCreatedAt;
	private String libraryUrl;
	private String docCenterUrl;
	private String format;
	private String teamsiteId;
	private String isDeleted;
	private String ownerId;
	private String isPublished;
	private List<AssignedToProfiles> assignedToProfiles;
	private List<PropertiesModel> proprties;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(String modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	public String getLatestLibraryCntntVersionId() {
		return latestLibraryCntntVersionId;
	}
	public void setLatestLibraryCntntVersionId(String latestLibraryCntntVersionId) {
		this.latestLibraryCntntVersionId = latestLibraryCntntVersionId;
	}
	public int getLatestLibraryCntntVersionSize() {
		return latestLibraryCntntVersionSize;
	}
	public void setLatestLibraryCntntVersionSize(int latestLibraryCntntVersionSize) {
		this.latestLibraryCntntVersionSize = latestLibraryCntntVersionSize;
	}
	public String getLatestLibraryCntntVersionCreatedAt() {
		return latestLibraryCntntVersionCreatedAt;
	}
	public void setLatestLibraryCntntVersionCreatedAt(String latestLibraryCntntVersionCreatedAt) {
		this.latestLibraryCntntVersionCreatedAt = latestLibraryCntntVersionCreatedAt;
	}
	public String getLibraryUrl() {
		return libraryUrl;
	}
	public void setLibraryUrl(String libraryUrl) {
		this.libraryUrl = libraryUrl;
	}
	public String getDocCenterUrl() {
		return docCenterUrl;
	}
	public void setDocCenterUrl(String docCenterUrl) {
		this.docCenterUrl = docCenterUrl;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getTeamsiteId() {
		return teamsiteId;
	}
	public void setTeamsiteId(String teamsiteId) {
		this.teamsiteId = teamsiteId;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getIsPublished() {
		return isPublished;
	}
	public void setIsPublished(String isPublished) {
		this.isPublished = isPublished;
	}
	public List<AssignedToProfiles> getAssignedToProfiles() {
		return assignedToProfiles;
	}
	public void setAssignedToProfiles(List<AssignedToProfiles> assignedToProfiles) {
		this.assignedToProfiles = assignedToProfiles;
	}
	public List<PropertiesModel> getProprties() {
		return proprties;
	}
	public void setProprties(List<PropertiesModel> proprties) {
		this.proprties = proprties;
	} 
	
}
