/**
 * 
 */
package com.metagenics.seismic.reports.model;

/**
 * @author PenchalaiahDasari
 *
 */
public class ContentViewHistoryModel {
	
	private String id;
	private String occurredAt;
	private String action;
	private String userId;
	private String instanceName;
	private String workspaceCntntId;
	private String workspaceCntntVersionId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOccurredAt() {
		return occurredAt;
	}
	public void setOccurredAt(String occurredAt) {
		this.occurredAt = occurredAt;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getWorkspaceCntntId() {
		return workspaceCntntId;
	}
	public void setWorkspaceCntntId(String workspaceCntntId) {
		this.workspaceCntntId = workspaceCntntId;
	}
	public String getWorkspaceCntntVersionId() {
		return workspaceCntntVersionId;
	}
	public void setWorkspaceCntntVersionId(String workspaceCntntVersionId) {
		this.workspaceCntntVersionId = workspaceCntntVersionId;
	}
	
}
