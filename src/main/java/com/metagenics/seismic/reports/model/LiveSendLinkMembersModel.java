package com.metagenics.seismic.reports.model;

public class LiveSendLinkMembersModel {
	
	
	private String livesendLinkId;
	private String email;
	private String modifiedAt;
	
	public String getLivesendLinkId() {
		return livesendLinkId;
	}
	public void setLivesendLinkId(String livesendLinkId) {
		this.livesendLinkId = livesendLinkId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getModifiedAt() {
		return modifiedAt;
	}
	public void setModifiedAt(String modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	
	
		
}
