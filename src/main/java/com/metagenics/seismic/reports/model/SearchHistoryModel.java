package com.metagenics.seismic.reports.model;

public class SearchHistoryModel {
	
	private String id;
	private String searchCycleId;
	private String searchTermRaw;
	private String searchTermNormalized;
	private String searchType;
	private String sortBy;
	private String occurredAt;
	private String userId;
	private String resultCount;
	private String resultCountDocCenter;
	private String resultCountWorkspace;
	private String resultCountContentManager;
	private String selectedFacetsInternal20Only;
	private String selectedFacetsTeam;
	private String selectedFacetsTopic;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSearchCycleId() {
		return searchCycleId;
	}
	public void setSearchCycleId(String searchCycleId) {
		this.searchCycleId = searchCycleId;
	}
	public String getSearchTermRaw() {
		return searchTermRaw;
	}
	public void setSearchTermRaw(String searchTermRaw) {
		this.searchTermRaw = searchTermRaw;
	}
	public String getSearchTermNormalized() {
		return searchTermNormalized;
	}
	public void setSearchTermNormalized(String searchTermNormalized) {
		this.searchTermNormalized = searchTermNormalized;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getOccurredAt() {
		return occurredAt;
	}
	public void setOccurredAt(String occurredAt) {
		this.occurredAt = occurredAt;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getResultCount() {
		return resultCount;
	}
	public void setResultCount(String resultCount) {
		this.resultCount = resultCount;
	}
	public String getResultCountDocCenter() {
		return resultCountDocCenter;
	}
	public void setResultCountDocCenter(String resultCountDocCenter) {
		this.resultCountDocCenter = resultCountDocCenter;
	}
	public String getResultCountWorkspace() {
		return resultCountWorkspace;
	}
	public void setResultCountWorkspace(String resultCountWorkspace) {
		this.resultCountWorkspace = resultCountWorkspace;
	}
	public String getResultCountContentManager() {
		return resultCountContentManager;
	}
	public void setResultCountContentManager(String resultCountContentManager) {
		this.resultCountContentManager = resultCountContentManager;
	}
	public String getSelectedFacetsInternal20Only() {
		return selectedFacetsInternal20Only;
	}
	public void setSelectedFacetsInternal20Only(String selectedFacetsInternal20Only) {
		this.selectedFacetsInternal20Only = selectedFacetsInternal20Only;
	}
	public String getSelectedFacetsTeam() {
		return selectedFacetsTeam;
	}
	public void setSelectedFacetsTeam(String selectedFacetsTeam) {
		this.selectedFacetsTeam = selectedFacetsTeam;
	}
	public String getSelectedFacetsTopic() {
		return selectedFacetsTopic;
	}
	public void setSelectedFacetsTopic(String selectedFacetsTopic) {
		this.selectedFacetsTopic = selectedFacetsTopic;
	}

	
}
